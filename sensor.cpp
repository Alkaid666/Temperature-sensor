#include<iostream>
#include<stdio.h>
#include<WinSock2.h>
#include<string.h>
#include<vector>
#include<sys/types.h>
#include<algorithm>
#include<ws2tcpip.h>
#include<ctime>
using namespace std;
#pragma comment (lib, "ws2_32.lib")  
#pragma warning(suppress : 4996)
#define HOST_IP "127.0.0.1"  
#define maxsize 50
constexpr auto HOST_PORT = 8080;
#define TH 85
#define TL -10
class Clock
{
public:
	int count; // 时钟计数单位
	void opentimer(); //开始计时
	void closetimer(); //结束计时
	int readtime(); //读取时间
	int writetime(int value); //改写count的值为value
	void forward(); //将计数单位增加一个单位
	void backward(); //将计数单位减少一个单位
};

class Transducer
{
public:
	friend class InterpreterService;
	int id;
	char name[32];
	double seed;
	double values[50]; /*温度传感器模块，id为该温度传感器的序号，name为传感器的名字，seed为用户选择的测量精度，values为测得温度值*/
	double read();
	void init();
private:
	void generate();
};
void Transducer::init() /*对传感器进行初始化*/
{
	seed = 0.5;//默认初始化后精度为0.5;
	strcpy_s(name, "0");
	for (int i = 0; i < 50; i++)
		values[i] = 0;
}
class TransducerSeverice
{
	Transducer trans[10];
	int getid(Transducer a)
	{
		return a.id;
	}
	void init()
	{
		for (int i = 0; i < 10; i++)
			trans[i].init();
	}
};
double Transducer::read() /*读取温度值*/
{
	generate();
	return *values;
}
void Transducer::generate()
{
	for (int i = 0; i < 50; i++)
	{
		values[i] = rand() % (-22, 50);
		values[i] /= seed;
	}
}
class ByteQueue
{
public:
	int head; //ByteQuene为循环队列，head为队首指针
	int rear; //队尾指针
	bool isempty; //判断是否队满
	bool isfull; //判断是否队空
	char* buff;
	void open(int capacity); //创建一个长度为capacity的数组
	void close(); //释放内存
	int pushback(char ch); //
	int popfront(char* ch);
	int writeback(char* buf, int len);
	int readfront(char* buf, int capacity);
	bool isempty1();
	bool isfull1();
};
class InterpreterService
{
private:
	Clock CLOCK;
public:
	void openinterpreter(Clock* clock); //开始翻译
	void closeinterpreter(Clock* clock); //翻译结束
	void interpreter(char* input, int len, char* output, Transducer* GTA); //将输入的长度为len的数组input按一定规则翻译成长度为capacity的数组output
};
struct sockaddr_in server;
struct sockaddr_in client;
int main(void)
{
	Transducer sensorgroup[10];
	srand((unsigned)time(NULL));
	int version_a = 1;
	int version_b = 1;
	WORD versionRequest = MAKEWORD(version_a, version_b);
	WSAData wsaData;
	int err;
	err = WSAStartup(versionRequest, &wsaData);
	if (err != 0)
	{
		printf("ERROR!");
		return -1;
	} //check whether the version is 1.1, if not, print the error and cleanup WSA
	if (LOBYTE(wsaData.wVersion) != 1 || HIBYTE(wsaData.wVersion) != 1)
	{
		printf("WRONG WINSOCK VERSION!");
		WSACleanup();
		return -1;
	}
	int sockfd;

	socklen_t len;
	int num;
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd == -1)
	{
		perror("Creating socket failed.\n");
		exit(1);
	}
	memset(&server, 0, sizeof(server)); //套接字清零
	server.sin_family = AF_INET;
	server.sin_port = htons(HOST_PORT);
	server.sin_addr.s_addr = inet_addr("127.0.0.1");
	if (bind(sockfd, (struct sockaddr*)&server, sizeof(server)) == -1)
	{
		perror("Bind() error.\n");
		return -1;
	}
	int retval = 0;
	len = sizeof(client);
	char send[maxsize] = { 'c' };
	char receive[1000] = { 0 };
	char readsensor[maxsize] = { 0 };
	char  databuf[maxsize] = { 0 };
	time_t start, now;
	start = time(NULL);
	while (1)
	{
		cout << 1;
		memset(receive, 0, maxsize);
		memset(send, 0, maxsize);
		memset(databuf, 0, maxsize);
		num = recvfrom(sockfd, receive, 1000, 0, (struct sockaddr*)&client, &len); //接收远程主机Socket传来的数据，并将其存入receive所指的内存空间
		if (num < 0)
		{
			perror("recvfrom() error.\n");
			closesocket(sockfd);
			WSACleanup();
			return -1;
		}
		else
		{
			cout << "sucessfully receive" << endl;


		}
		receive[strlen(receive)] = '\0';
		memcpy(readsensor, receive, 10);
		ByteQueue buffqueue;
		buffqueue.open(size(receive));
		char* read_sensor = buffqueue.buff;
		memset(read_sensor, 0, 50);
		int i = 0;
		buffqueue.readfront(receive, strlen(receive));
		cout << readsensor << endl;

		InterpreterService interpreter;
		interpreter.interpreter(readsensor, strlen(readsensor) + 1, send, sensorgroup);



		if (strcmp(readsensor, "read") == 0) {
			for (int i = 0; i < 10; i++) {
				sensorgroup[i].read();
				cout << sensorgroup[i].values;
				for (int j = 0; j < maxsize; j++)
				{
					if (sensorgroup[i].values[j] > 85 || sensorgroup[i].values[j] < -25)
					{
						sprintf_s(send, "the temperature is abnormal");

					}
				}
			}
			sprintf(databuf, (const char*)sensorgroup[0].values);
			cout << databuf;

			now = time(NULL);

			retval = sendto(sockfd, databuf, strlen(databuf) + 1, 0, (const sockaddr*)&client, sizeof(sockaddr));

		}

		retval = sendto(sockfd, send, strlen(send) + 1, 0, (const sockaddr*)&client, sizeof(sockaddr));


	}

	closesocket(sockfd);
	WSACleanup();
	return 0;

}
void ByteQueue::open(int capacity)
{
	buff = (char*)malloc(capacity);
} //创建有catacity个数组的字符串数组buff
void ByteQueue::close() {
	free(buff);
}
int ByteQueue::pushback(char ch) //入队操作，将字符ch写入数组buff中
{
	rear = (rear + 1) % sizeof(buff);
	if (rear == head)
	{
		isfull = 1;
		rear = (rear - 1 + sizeof(buff)) % sizeof(buff);
		return 0;
	}
	buff[rear] = ch;
	return 1;
}
int ByteQueue::popfront(char* ch) //出队操作，将字符ch从数组buff中移出
{
	if (isempty == 1)
		return 0;
	head = (head + 1) % sizeof(buff);
	int front = buff[head];
	if (head == rear)
		isempty = 1;
	return front;
}
int ByteQueue::writeback(char* buf1, int len) //将字符串数组buf1中长度为len的字符写入，若成功返回1
{
	for (int i = 0; i < len; i++) {
		rear = (rear + 1) % sizeof(buff);
		if (rear == head) {
			isfull = 1;
			rear = (rear - 1 + sizeof(buff)) % sizeof(buff);
			return 0;
		}
		buff[rear] = *buf1;
		buf1 = buf1 + 1;
	}
	return 1;
}
int ByteQueue::readfront(char* buf2, int capacity) //读取数组的前capacity个字符并写入buf2中，若成功返回1
{
	for (int i = 0; i < capacity; i++)
	{
		if (isempty == 1)
			return 0;
		head = (head + 1) % sizeof(buff);
		*buf2 = buff[head];
		buf2 = buf2 + 1;
		if (head == rear)
			isempty = 1;
	}
	return 1;
}
bool ByteQueue::isempty1()
{
	if (isempty == 1)
		return 1;
	else
		return 0;
}
bool ByteQueue::isfull1()
{
	if (isfull == 1)
		return 1;
	else
		return 0;
}
void Clock::opentimer()
{
	clock_t start;
	start = clock();
}
void Clock::closetimer()
{
	clock_t finish;
	finish = clock();
}
void Clock::forward()
{
	count++;
}
void Clock::backward()
{
	count--;;
}
int Clock::readtime()
{
	return count;
}
int Clock::writetime(int value)
{
	count = value;
	return count;
}
void InterpreterService::openinterpreter(Clock* clock)
{

}
void InterpreterService::closeinterpreter(Clock* clock)
{

}
void InterpreterService::interpreter(char* input, int len, char* send, Transducer* GTA)//将用户输入的文字指令转化为传感器可识别的机器指令
{

	if (strcmp(input, "chooseA") == 0) //用户指令为精度模式A
	{
		for (int i = 0; i < 10; i++)
			GTA[i].seed = 0.0625;
		sprintf(send, "you have chosen 0.0625");
		cout << "you have chosen 0.0625" << endl;
	}
	else if (strcmp(input, "chooseB") == 0) //用户指令为精度模式B
	{
		for (int i = 0; i < 10; i++)
			GTA[i].seed = 0.5;
		sprintf(send, "you have chosen 0.5");
	}
	else if (strcmp(input, "chooseC") == 0)//用户指令为精度模式C
	{
		for (int i = 0; i < 10; i++)
			GTA[i].seed = 0.25;
		sprintf(send, "you have chosen 0.25");
	}
	else if (strcmp(input, "chooseD") == 0)//用户指令为精度模式D
	{
		for (int i = 0; i < 10; i++)
			GTA[i].seed = 0.125;
		sprintf(send, "you have chosen 0.125");
	}
	else if (strcmp(input, "read") == 0)
	{
		for (int i = 0; i < 10; i++) {
			GTA[i].read();
			cout << GTA[i].values << ' ';
			for (int j = 0; j < maxsize; j++)
			{
				if (GTA[i].values[j] > 125 || GTA[i].values[j] < -25)
				{
					sprintf(send, "the temperature is abnormal");
					cout << "the temperature is abnormal" << endl;
				}
			}
		}
	}
	else if (strcmp(input, "reset") == 0)
	{
		for (int i = 0; i < 10; i++)
			GTA[i].init();
		sprintf(send, "reset finsh");
		cout << "reset finish" << endl;
	}
	else
		sprintf(send, "Invalid message");
}