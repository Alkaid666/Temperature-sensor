#include<winsock2.h>
#include<stdio.h>
#include<string.h>
#include<iostream>
#include<queue>
using namespace std;
#pragma comment(lib,"ws2_32.lib")
# define BUFFER_SIZE  1024    //缓冲区大小

struct DHT11 {
	double TEMP, HUMI;
};

struct MQ2 {
	int XLEL, TTLD;
};

/*  DHT11发送的是二进制的数据:8位温度小数+8位温度整数+8位湿度小数+8位湿度整数；该函数用于把收到的二进制数据转化成10进制  */
double trans(queue<char>& DHT11_data) {
	int INT = 0;
	double DEC = 0, ans;
	for (int i = 0; i < 8; i++) {
		int bit = DHT11_data.front();
		bit = bit - '0';
		DHT11_data.pop();
		DEC = DEC + bit * pow(2, i);
	}
	for (int i = 0; i < 8; i++) {
		int bit = DHT11_data.front();
		bit = bit - '0';
		DHT11_data.pop();
		INT = INT + (int)bit * pow(2, i);
	}
	if (DEC < 10)
		DEC = DEC * 0.1;
	else if (DEC < 100)
		DEC = DEC * 0.01;
	else
		DEC = DEC * 0.001;
	ans = (double)INT + DEC;
	return ans;
}

/*MQ2发送的是离散化的模拟值和二进制数据：0-1023*/


int main() {
	SOCKET sock_Client; //客户端用于通信的Socket
	WSADATA WSAData;
	char  receBuf_init[BUFFER_SIZE], receBuf_read[BUFFER_SIZE], sendBuf_init[BUFFER_SIZE], sendBuf_read[BUFFER_SIZE];

	if (WSAStartup(MAKEWORD(2, 2), &WSAData) != 0) {
		printf("初始化失败!");
		return -1;
	}
	sock_Client = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);//创建客户端用于通信的Socket
	SOCKADDR_IN addr_server;
	addr_server.sin_family = AF_INET;
	addr_server.sin_port = htons(4567);
	addr_server.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
	SOCKADDR_IN sock;
	int len = sizeof(sock);

	queue<char> DHT11_data;

	/*  对传感器进行初始化  */
	while (true) {
		cout << "The command to be executed:";
		cin >> sendBuf_init;
		if (strcmp(sendBuf_init, "init") != 0) {
			cout << "Sensors haven't been initialized. Please initialize first!" << endl;
			continue;
		}
		break;
	}
	sendto(sock_Client, sendBuf_init, strlen(sendBuf_init), 0, (SOCKADDR*)&addr_server, sizeof(SOCKADDR));
	int last = recvfrom(sock_Client, receBuf_init, strlen(receBuf_init), 0, (SOCKADDR*)&sock, &len);
	if (last > 0) {
		if (strcmp(receBuf_init, "DHT11 and MQ2 initialized.")) {
			cout << "DHT11 and MQ2 initialized." << endl;
		}
	}

	/*  读取传感器的数据  */
	while (true) {
		cout << "The command to be executed:";
		cin >> sendBuf_read;
		if ((strcmp(sendBuf_read, "read1") != 0)&& (strcmp(sendBuf_read, "read2") != 0)) {
			cout << "Unrecognized command." << endl << "The commands you can enter are:" << endl << "read temperature and read concentration " << endl;
			continue;
		}
		break;
	}

	while (true) {
		if (strcmp(sendBuf_read, "read1") == 0) {
			for (int i = 0; i < 4; i++) {
				sendto(sock_Client, sendBuf_read, strlen(sendBuf_read), 0, (SOCKADDR*)&addr_server, sizeof(SOCKADDR));
				int last = recvfrom(sock_Client, receBuf_read, strlen(receBuf_read), 0, (SOCKADDR*)&sock, &len);
				if (last > 0) {
					receBuf_read[last] = '/0';
					for (int i = 0; i < 8; i++)   //要替换成自己写的队列
						DHT11_data.push(receBuf_read[i]);
				}
			}

			for (int i = 0; i < 4; i++) {
				sendto(sock_Client, sendBuf_read, strlen(sendBuf_read), 0, (SOCKADDR*)&addr_server, sizeof(SOCKADDR));
				int last = recvfrom(sock_Client, receBuf_read, strlen(receBuf_read), 0, (SOCKADDR*)&sock, &len);
				if (last > 0) {
					receBuf_read[last] = '/0';
					int MQ2_AOUT; bool MQ2_DOUT;
				}
			}

			/*  得到本次温湿度数据  */
			DHT11 data;
			data.TEMP = trans(DHT11_data);
			data.HUMI = trans(DHT11_data);

			/*  判断收到的数据是否有误 温度范围：0~50 湿度范围：20%~95%  */
			if (data.TEMP >= 0 && data.TEMP <= 50 && data.HUMI >= 20 && data.HUMI <= 95) {
				cout << data.TEMP << " " << data.HUMI << endl;
				system("pause");
			}
		}
		else if (strcmp(sendBuf_read, "read2") == 0) {
			MQ2 data2;
			data2.XLEL = 0;
			data2.TTLD = 0;

			sendto(sock_Client, sendBuf_read, strlen(sendBuf_read), 0, (SOCKADDR*)&addr_server, sizeof(SOCKADDR));
			int last = recvfrom(sock_Client, receBuf_read, strlen(receBuf_read), 0, (SOCKADDR*)&sock, &len);
			if (last > 0) {
				receBuf_read[last] = '/0';
				for (int i = 0; i < 4; i++)   //要替换成自己写的队列
					data2.XLEL += int(receBuf_read[i]) * pow(10, (3 - i));
				data2.TTLD = receBuf_read[4];
			}
			cout <<"The smoke concentration index is  "<< data2.XLEL << ". "  << endl;
			if (data2.TTLD == 1)
				cout << "The smoke is not thick." << endl;
			else
				cout << "The smoke is thick." << endl;
			
		}
	}
	closesocket(sock_Client);
	WSACleanup();

	system("pause");
	return 0;
}